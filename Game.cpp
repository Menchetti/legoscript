/**
* @version 1.1
* @date 18-Nov-2020
*
* Edited by Aidan Sheedy
* - Added aditional includes, temporarily launching from gamestate for testing purposes
* - Changed formating
* - Removed passing of interpolation from the call to state->draw during. I don't know exactly why this was in there, but from looking
*   around in the tutorial files, it doesn't look like we need it.
* - Properly removed all menitions to dt and replaced with TICK_RATE
*
* @version 1.0
* @date 31-Oct-2020
*
* Created by Chloe Talman
**/

#include "Game.hpp"
//#include "LaunchState.hpp"
#include "LaunchState.hpp" // Temporary

#include <stdlib.h>
#include <time.h>
#include "DEFINITIONS.hpp"


using namespace std;


Game::Game(int width, int height, std::string title) {
	srand(time(NULL));
	gameData->window.create(sf::VideoMode(width, height), title, sf::Style::Close | sf::Style::Titlebar);
	gameData->stateManager.addState(stateRef(new LaunchState(this->gameData)));
	this->Run();
}

void Game::Run() {
	float newTime, frameTime, interpolation;
	float currentTime = this->clock.getElapsedTime().asSeconds();
	float accumulator = 0.0f;

	while (this->gameData->window.isOpen()) {
		this->gameData->stateManager.processStateChanges();

		newTime = this->clock.getElapsedTime().asSeconds();
		frameTime = newTime - currentTime;

		if (frameTime > 0.25f) {
			frameTime = 0.25f;
		}
		currentTime = newTime;
		accumulator += frameTime;

		while (accumulator >= TICK_RATE) {
			this->gameData->stateManager.getActiveState()->manageInput();
			this->gameData->stateManager.getActiveState()->update();

			accumulator -= TICK_RATE;
		}

		// interpolation = accumulator / TICK_RATE;	// I DON'T KNOW IF WE NEED THIS OR NOT I'M GONNA TAKE IT OUT FOR NOW
		this->gameData->stateManager.getActiveState()->draw();
	}
}