/**
* @version 1.1
* @date	   27-Nov-2020
*
*Edited by Aidan Sheedy
* - Fixed some variable names to align with UML Class Diagram
* - Made entities pointers
* - Made test variable a parameter to allow for class-wide scope
*
* @version 1.0
* @date    22-Nov-2020
*
* Created by Alessandro Menchetti
*/

#pragma once

#include "Group.hpp"
#include "Mask.hpp"
#include "Virus.hpp"

using namespace std;

/**
* The EntityGenerator class will generate the entities used by the GameState class randomly or based off of current score/speed (have not figured this out yet)
* 
*/
class EntityGenerator {
public:
	/**
	* Constructor for a EntityGenerator object, takes in an argument of type GameDataRef for information on the current game.
	*
	* @param data - GameDataRef struct with information on the current game
	*
	*/
	EntityGenerator();

	/**
	* Spawns entities into the game.
	* 
	* @param mask - Mask object to be calling object for its spawn function
	* @param virus - Virus object to be calling object for its spawn function
	* @param group - Group object to be calling object for its spawn function
	*/
	void spawnEntities(Mask* mask, Virus* virus, Group* group);

	void pause();

	void play();

private:
	sf::Clock clock;
	float pauseTime;
	float currentTime;
	Mask* mask;
	Virus* virus;
	Group* group;

	int test = 0;
};
