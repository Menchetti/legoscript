/*
*
* Created by: Alex Koch
*/


#include <sstream>
#include "DEFINITIONS.hpp"
#include "GameOverState.hpp"
#include "GameState.hpp"
#include "MainMenuState.hpp"

#include <iostream>
#include <fstream>

using namespace std;

    GameOverState::GameOverState(GameDataRef data, int score, int mask) : localGameData(data), score(score), mask(mask)
    {
        
    }
    
    void GameOverState::init()
    {
        //omission of leaderboard functionality
        //only implements the latest score by printing to the screen

        this->localGameData->assetManager.loadTexture("Game Over Screen", GAME_OVER_SCREEN_FILEPATH);
        this->localGameData->assetManager.loadTexture("Back Button", BACK_BUTTON_FILEPATH);
        this->localGameData->assetManager.loadTexture("Play Button", PLAY_BUTTON_FILEPATH);
        this->localGameData->assetManager.loadFont("Arial", FONT_FILE_PATH);

        gameOverScreen.setTexture(this->localGameData->assetManager.getTexture("Game Over Screen"));
        backButton.setTexture(this->localGameData->assetManager.getTexture("Back Button"));
        playButton.setTexture(this->localGameData->assetManager.getTexture("Play Button"));

        backButton.setPosition(947, 380); //should be exact pixel placement
        playButton.setPosition(920, 260); //should be exact pixel placement

        scoreText.setFont(this->localGameData->assetManager.getFont("Arial"));
        scoreText.setString(std::to_string(score));
        scoreText.setCharacterSize(65);
        scoreText.setFillColor(sf::Color::White);
        scoreText.setOrigin(sf::Vector2f(scoreText.getGlobalBounds().width / 2, scoreText.getGlobalBounds().height / 2));
        scoreText.setPosition(175, 270);

        maskText.setFont(this->localGameData->assetManager.getFont("Arial"));
        maskText.setString(std::to_string(mask));
        maskText.setCharacterSize(65);
        maskText.setFillColor(sf::Color::White);
        maskText.setOrigin(sf::Vector2f(maskText.getGlobalBounds().width / 2, maskText.getGlobalBounds().height / 2));    //center at 184
        maskText.setPosition(175, 430);
    }
    
    void GameOverState::manageInput()
    {
        sf::Event event;
        
        while (this->localGameData->window.pollEvent(event))
        {
            if (sf::Event::Closed == event.type)
            {
                this->localGameData->window.close();
            }
            
            if (this->localGameData->inputManager.isSpriteClicked(this->backButton, sf::Mouse::Left, this->localGameData->window))
            {
                this->localGameData->stateManager.addState(stateRef(new MainMenuState(localGameData)), true);
            }

            if (this->localGameData->inputManager.isSpriteClicked(this->playButton, sf::Mouse::Left, this->localGameData->window))
            {
                this->localGameData->stateManager.addState(stateRef(new GameState(localGameData)), true);
            }
        }
    }
    
    void GameOverState::update()
    {
        
    }
    
    void GameOverState::draw()
    {
        this->localGameData->window.clear(sf::Color::Black);
        localGameData->window.draw(gameOverScreen);
        localGameData->window.draw(backButton);
        localGameData->window.draw(playButton);
        localGameData->window.draw(scoreText);
        localGameData->window.draw(maskText);
                
        this->localGameData->window.display();
    }