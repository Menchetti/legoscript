/**
* @version 1.0
* @date    29-Oct-2020
* 
* Created by Alessandro Menchetti
*/

#pragma once

#include <memory>
#include <stack>

#include "State.hpp"

using namespace std;

typedef unique_ptr<State> stateRef; //Pointer to a state object 

/**
* The StateMachine class handles the different states that the program can be in, such as main menu, game, pause menu, etc.
* 
* This class handles the different states the program can be in using a stack. A state can be added to the top of the stack to allow for easy acess to the previous state (just pop the top one) as would be desired with having a pause menu state (the game state would not be destroyed, the
* pause state would simply be added to the top of the stack and popped when the game either resumes, or goes back to main menu). The class has member functions addState and removeState to add and remove states, as well as processStateChanges to be called in the game loop and and accessor
* getActiveState that returns a state reference to the current active state.
*/
class StateManager {
public:
	
	/**
	* Constructor for a StateMachine object
	*/
	StateManager() { }

	/**
	* Destructor for a StateMachine object
	*/
	~StateManager() { }

	/**
	* The addState method changes the private boolean variables to tell the processStateChanges method if a state is being added to the stack or replacing the current state (_isAdding to true and _isReplacing to whatever is passed in as the second parameter)
	* 
	* @param newState - a stateRef of the state that will be "added"
	* @param isReplacing - A boolean to indicate if the state to be added is replacing a current one, or just being added to the stack. Initialized to true as default
	*/
	void addState(stateRef newState, bool isReplacing = true);
	
	/**
	* The removeState method changes the private boolean variable _isRemoving to true so the processStateChanges method can do the required operations to remove a state from the stack
	*/
	void removeState();

	/**
	* The processStateChanges method handles the actual stack that holds the states. It checks the three private boolean variables and executes operation based off of these variables. 
	*/
	void processStateChanges();

	/**
	* The getActiveState method is an accessor to the active state. It returns whichever state is at the top of the stack.
	*/
	stateRef &getActiveState();

private:
	stack<stateRef> _states;
	stateRef _newState;

	bool _isRemoving;
	bool _isAdding;
	bool _isReplacing;
};

