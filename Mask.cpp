/**
* @version 1.2
* @date    27-Nov-2020
*
* Edited by Aidan Sheedy
* - Fixed spawning positioning
* - Implemented use of Isometric movement
* - Implemented deleteMask() function
* 
* @version 1.1
* @date    22-Nov-2020
* 
* Edited by Aidan Sheedy
* - removed constructor
* - implemented init()
* - fixed some names to reflect Class Diagram
*
* Created by Alessandro Menchetti
*/

#include "Mask.hpp"
#include "DEFINITIONS.hpp"

using namespace std;

void Mask::spawn() {
	sprite.setPosition(916 + floatingYOffset * ISOMETRIC_RATIO, 794 - floatingYOffset * (1 - ISOMETRIC_RATIO) - sprite.getTexture()->getSize().y);
	sprites.push_back(sprite);
}

void Mask::init() {
	this->localGameData->assetManager.loadTexture("Mask Texture", MASK_FILEPATH);
	maskTexture = this->localGameData->assetManager.getTexture("Mask Texture");
	sprite.setTexture(maskTexture);
}

// Deletes the mask at the provided indice
void Mask::deleteMask(int indice) {
	sprites.erase(sprites.begin() + indice);
}