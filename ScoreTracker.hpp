/*
* @version 1.1
* @date    27-Nov-2020
*
* Edited by Aidan Sheedy
* - Cleaned up include statements
* - Removed unnecesary localGameData parameter
* 
* @version 1.0
* @date	 25-nov-2020
* 
* Created by: Chloe Talman
*/
#pragma once

#include <string>
#include <vector>;
#include "DEFINITIONS.hpp"

using namespace std;

class ScoreTracker{
public:
	ScoreTracker();
	void init();
	void update(float currentTime, int newMasks);
	void saveScore(string userName);
	void updateHighScore();
	int getScore();
	int getNumMasks();

private:
	int score;
	int numMasks;
	string username;
	int previousTime;
	vector<string> scores;    //scores read from file of high scores
	vector<string> userNames; //usernames read from file of high scores

};


