/**
* @version 1.2
* @date    27-Nov-2020
*
* Edited by Aidan Sheedy
* - Fixed spawning positioning
* - Implemented use of Isometric movement
* - Added use of debugging definition to show hitboxes
* - Implemented playerHitbox sprite to allow for smooth animation and proper collision control
* 
 * @Version 1.1
 * @Date 14-Nov-2020
 *
 * Edited by Aidan Sheedy
 * - Added temproary texture loading - might be removed if a sprite sheet is introduced
 * - Included use of the jumping, ducking, and still textures
 * - added update() function
 * - removed unnecessary else statement in isDucking()
 *
 * Created by Alex Koch
 **/

#include "Player.hpp"

#include <iostream>

using namespace std;

	Player::Player(GameDataRef gameData) : localGameData(gameData)
	{
		// Load in textures
		this->localGameData->assetManager.loadTexture("Player Frame 1", PLAYER_FRAME_1_FILEPATH);
		this->localGameData->assetManager.loadTexture("Player Frame 2", PLAYER_FRAME_2_FILEPATH);
		this->localGameData->assetManager.loadTexture("Player Frame 3", PLAYER_FRAME_3_FILEPATH);
		this->localGameData->assetManager.loadTexture("Player Frame 4", PLAYER_FRAME_4_FILEPATH);
		this->localGameData->assetManager.loadTexture("Player Ducking", PLAYER_DUCKING_FILEPATH);
		this->localGameData->assetManager.loadTexture("Player Still", PLAYER_STILL_FILEPATH);
		this->localGameData->assetManager.loadTexture("Player Jumping", PLAYER_JUMPING_FILEPATH);

		//not sire about this one either, depends on the amount of frames Joe made
		animationIterator = 0;

		duckingTexture = this->localGameData->assetManager.getTexture("Player Ducking");
		stillTexture = this->localGameData->assetManager.getTexture("Player Still");
		jumpingTexture = this->localGameData->assetManager.getTexture("Player Jumping");

		animationFrames.push_back(this->localGameData->assetManager.getTexture("Player Frame 1"));
		animationFrames.push_back(this->localGameData->assetManager.getTexture("Player Frame 2"));
		animationFrames.push_back(this->localGameData->assetManager.getTexture("Player Frame 3"));
		animationFrames.push_back(this->localGameData->assetManager.getTexture("Player Frame 4"));

		playerSprite.setTexture(stillTexture);

		playerSprite.setPosition(300, GROUND_HEIGHT);
		//subject to change as it depends on asset layout

		playerState = PLAYER_STATE_STILL;

		playerSprite.setOrigin(sf::Vector2f(13, 38));
	}

	Player::~Player()
	{
	}

	void Player::draw()
	{
		localGameData->window.draw(playerSprite);
		playerHitbox = playerSprite;

		switch (playerState) {
		case PLAYER_STATE_DUCKING:
			playerHitbox.move(-12, 20);
			break;
		case PLAYER_STATE_JUMPING:
			playerHitbox.move(27, -17);
			break;
		case PLAYER_STATE_RUNNING:
			playerHitbox.move(7, -17);
			break;
		} // end swith case

		playerHitbox.move(-40, -20);

#ifdef SHOW_HITBOXES
		sf::Vector2f texSize(playerHitbox.getTextureRect().width, playerHitbox.getTextureRect().height);
		sf::FloatRect hitbox = playerHitbox.getGlobalBounds();
		sf::RectangleShape textureRect(sf::Vector2f(hitbox.width, hitbox.height));
		textureRect.setOutlineThickness(2);
		textureRect.setOutlineColor(sf::Color::Black);
		textureRect.setFillColor(sf::Color::Transparent);
		//playerSprite.setOrigin(0, 0);
		textureRect.setPosition(playerHitbox.getPosition());
		this->localGameData->window.draw(textureRect);
#endif
	}

	// This works for now but there is definitely be a more elegant solution
	// Should probably have the texture setting in the functions that change the state (ie duck(), unDuck(), jump())
	void Player::update() {
		switch (playerState) {
		case PLAYER_STATE_DUCKING:
			playerSprite.setTexture(duckingTexture);
			playerSprite.setOrigin(sf::Vector2f(51, 0));
			break;
		case PLAYER_STATE_JUMPING:
			playerSprite.setTexture(jumpingTexture);
			playerSprite.setOrigin(sf::Vector2f(13, 38));
			if (velocity == 0)
				playerState = PLAYER_STATE_RUNNING;
			break;
		case PLAYER_STATE_RUNNING:
			this->animate();
			break;
		default:
			playerSprite.setTexture(stillTexture);
			break;
		} // end swith case
		playerSprite.setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(playerSprite.getTexture()->getSize())));
		if (velocity != 0 && playerState != PLAYER_STATE_DUCKING)
			playerState = PLAYER_STATE_JUMPING;
	} // end update()

	void Player::jump() 
	{
		if (velocity == 0) {
			playerState = PLAYER_STATE_JUMPING;
			setVelocity(PLAYER_JUMP_VELOCITY);
		}
	}

	void Player::duck(){
		playerState = PLAYER_STATE_DUCKING;
	}

	void Player::unDuck(){
		playerState = PLAYER_STATE_RUNNING;
	}

	void Player::move(float displacement)
	{
		playerSprite.move(0, displacement);
	}

	const sf::Sprite& Player::getSprite() const
	{
		return playerSprite;
	}

	const sf::Sprite& Player::getCollisionSprite() const
	{
		return playerHitbox;
	}

	void Player::setVelocity(float vel)
	{
		velocity = vel;
	}

	float Player::getVelocity()
	{
		return velocity;
	}

	bool Player::isDucking()
	{
		if (PLAYER_STATE_DUCKING == playerState) {
			return true;
		}
		return false;
	}

	void Player::animate()
	{
		if (clock.getElapsedTime().asSeconds() > PLAYER_ANIMATION_DURATION / animationFrames.size())
		{
			if (animationIterator < animationFrames.size() - 1)
			{
				animationIterator++;
			}
			else
			{
				animationIterator = 0;
			}
			if (animationIterator % 2 == 0) {
				playerSprite.setOrigin(sf::Vector2f(32, 38));
			}
			else {
				playerSprite.setOrigin(sf::Vector2f(29, 38));
			}
			playerSprite.setTexture(animationFrames.at(animationIterator));
			clock.restart();
		}
	}