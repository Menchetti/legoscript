/**
* @version 1.1
* @date 18-Nov-2020
*
* Edited by Aidan Sheedy
* - removed dt parameter
* - changed parameters to match class diagram naming
*
* @ Version 1.0
* @ Date 31-Oct-2020
*
* Created by Chloe Talman
**/


#pragma once

#include <memory>
#include <string>
#include <SFML/Graphics.hpp>
#include "StateManager.hpp"
#include "AssetManager.hpp"
#include "inputManager.hpp"

using namespace std;

struct GameData {
	StateManager stateManager;
	sf::RenderWindow window;
	AssetManager assetManager;
	InputManager inputManager;
};

typedef std::shared_ptr<GameData> GameDataRef;

class Game {

public:
	Game(int width, int height, std::string title); //creates the game window

private:
	sf::Clock clock; //handles framerate

	GameDataRef gameData = std::make_shared<GameData>();

	void Run(); //called when we start the game
};
