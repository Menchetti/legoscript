/**
* @version 1.1
* @date    21-Nov-2020
*
* Edited by Aidan Sheedy
* - Removed constructor, changed to use Entity structure for proper inheritance
* - Added initialization function to load textures in case a sprite sheet is not used
* - added several private attributes for the next sprite to spawn
* 
* @version 1.0
* @date    12-Nov-2020
*
* Created by Alessandro Menchetti
*/

#pragma once

#include "Entity.hpp"

using namespace std;

/**
* The Group class extends Entity and has its own constructor as well as a defined spawn() function, a function called RandomizeSize() and an added member variable _groupSize.
*
*/
class Group : public Entity {
	using Entity::Entity;

public:
	// TESTING FUNCTION - initializes textures in case of a lack of texture sheet
	void init();
	
	/**
	* The spawn() function simply spawns in a Mask object on the far right of the screen and uses the "Virus" sprite defined in the DEFINITIONS.hpp file.
	*
	*/
	void spawn();

	/**
	* The randomizeSize() function simply generates a random number between 1 and the specified number of possible group sizes to choose from and sets the variable _groupSize equal to this number.
	* 
	*/
	void randomizeSize();

private:
	// group size - dictates how the size of group to spawn next
	int groupSize;

	// Sprite and textures for the each group
	sf::Sprite spriteToSpawn;
	sf::Texture smallGroupTexture;
	sf::Texture mediumGroupTexture;
	sf::Texture largeGroupTexture;

};