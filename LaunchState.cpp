#include <sstream>
#include "LaunchState.hpp"
#include "DEFINITIONS.hpp"
#include "MainMenuState.hpp"

#include <iostream>

using namespace std;
LaunchState::LaunchState(GameDataRef data) : localGameData(data) { }

void LaunchState::init() {
	this->localGameData->assetManager.loadTexture("Splash Background", SPLASH_BACKGROUND_FILEPATH); //variable name will be a filepath in DEFINITIONS.hpp

	splashBackground.setTexture(this->localGameData->assetManager.getTexture("Splash Background"));
}

void LaunchState::manageInput() {
	sf::Event event;

	while (this->localGameData->window.pollEvent(event)) {
		if (sf::Event::Closed == event.type) {
			this->localGameData->window.close();
		}
	}
}

void LaunchState::update() {
	if (this->localClock.getElapsedTime().asSeconds() > SPLASH_SCREEN_SHOW_TIME) {
		this->localGameData->stateManager.addState(stateRef(new MainMenuState(localGameData)), true);
	}
}

void LaunchState::draw() {
	this->localGameData->window.clear(sf::Color::Black);
	this->localGameData->window.draw(this->splashBackground);
	this->localGameData->window.display();
}