#include "Game.hpp"
#include "DEFINITIONS.hpp"
#include "PhysicsEngine.hpp"
#include "BackgroundScroller.hpp"
#include <Windows.h>

using namespace std;


int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pCmdLine, int nCmdShow) {
//int main() {
	Game(WINDOW_WIDTH, WINDOW_HEIGHT, "Queen's Land");
	return EXIT_SUCCESS;
}