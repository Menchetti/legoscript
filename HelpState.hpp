/*
*
* Created by: Alex Koch
*/


#pragma once

#include <SFML/Graphics.hpp>
#include "HelpState.hpp"
#include "Game.hpp"

using namespace std; 

	class HelpState : public State
	{
	public:
		HelpState(GameDataRef gameData);
		void init();
		void manageInput();
		void update(); //doesn't do anything, implemented since its in the state interface
		void draw();
        void pause(); //doesn't do anything, implemented since its in the state interface
        void play(); //doesn't do anything, implemented since its in the state interface

	private:
		GameDataRef localGameData;

        sf::Sprite helpMenuScreen; //added since the help menu is a different asset than the background, the UML doesn't reflect this change
		sf::Sprite backButton;
//		sf::Sound helpMusic; //not initialized for time being, strong chance we don't get around to sounds 
//        sf::Sound buttonPressSound; //not initizalied for time being, stroung chance we don't get around to sounds
	};