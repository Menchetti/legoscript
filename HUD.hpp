/*
* @version 1.0
* @date	 25-nov-2020
*
* Created by: Chloe Talman
*/
#pragma once

#include <SFML/Graphics.hpp>

#include "DEFINITIONS.hpp"
#include "Game.hpp"

class HUD{
public:
	HUD();
	HUD(GameDataRef localGameData);
	~HUD();
	void draw();
	void update(int score, int numMasks, float time);
	void displayWaitText();

private:
	GameDataRef localGameData;
	int localScore;
	sf::Text scoreText;
	sf::Text timeText;
	sf::Text maskText;
	sf::Text waitText;
};


