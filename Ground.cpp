/**
* @version 1.1
* @date    27-Nov-2020
*
* Edited by Aidan Sheedy
* - Fixed spawning positioning
* - Implemented use of Isometric movement
* 
* @ Version 1.0
* @ Date 15-Nov-2020
*
* Created by Chloe Talman
**/


#include "Ground.hpp"
#include "DEFINITIONS.hpp"
#include "BackgroundScroller.hpp"

using namespace std;

Ground::Ground ( GameDataRef gameData) : localGameData(gameData) {

	this->localGameData->assetManager.loadTexture("Ground", GROUND_1_FILEPATH);

	sf::Sprite sprite(localGameData->assetManager.getTexture("Ground"));
	sf::Sprite sprite2(localGameData->assetManager.getTexture("Ground"));
	sf::Sprite sprite3(localGameData->assetManager.getTexture("Ground"));
	
	sprite.setPosition(-400,0);
	sprite2.setPosition(555, 551);
	sprite3.setPosition(1510, 1102);

	groundSprites.push_back(sprite);
	groundSprites.push_back(sprite2);
	groundSprites.push_back(sprite3);

}

void Ground::moveGround(int speed){
	for (unsigned short int i = 0; i < groundSprites.size(); i++) {
		float movement = speed * TICK_RATE;
		groundSprites.at(i).move(-movement * ISOMETRIC_RATIO, -movement * (1 - ISOMETRIC_RATIO));

		if (groundSprites.at(i).getPosition().x < 0 - groundSprites.at(i).getGlobalBounds().width) {
			groundSprites.at(i).setPosition(GROUND_X_SPAWN, GROUND_Y_SPAWN);
			groundSprites.push_back(groundSprites.at(i));
			groundSprites.erase(groundSprites.begin());
		}
	}

}

void Ground::drawGround() {
	for (unsigned short int i = 0; i < groundSprites.size(); i++) {
		localGameData->window.draw(groundSprites.at(i));
	}
}