/**
* @version 1.2
* @date	   27-Nov-2020
* 
* Edited by Aidan Sheedy
* - Made draw() virutal
* 
* @version 1.1
* @date    21-Nov-2020
*
* Edited by Aidan Sheedy
* - Added constructor that sets the localGameData parameter
* - renamed parameters to match Class Diagram
* - changed parameter of move to take the speed at which the ground is moving
* 
* @version 1.0
* @date    12-Nov-2020
*
* Created by Alessandro Menchetti
*/

#pragma once

#include <SFML/Graphics.hpp>
#include "Game.hpp"
#include <vector>

using namespace std;

/**
* The Entity class is the base class for any entity in the game.
* 
* It holds all the methods that every entity will have such as a spawn function, move and draw as well as a &GetSprites function. Its member variables include a reference to the current game data, a vector of type sprites to hold the sprite files for each entity, and landheight.
* 
*/
class Entity {

public:
	// Entity constructor
	Entity(GameDataRef data);

	/**
	* The spawn method is a purely virtual method as it must be defined in its own way specific to the child class implementing it.
	* 
	*/
	virtual void spawn() = 0;

	/**
	* The move function takes in the time between frames to ensure any entity moves at constant speed regardless of framerate. For every sprite in the vector it first checks to see if they have moved off screen and if so, they are removed from the vector. If not, the sprite is moved
	* at the specified speed (MOVEMENT_SPEED) from the right of the screen to the left.
	* 
	* @param speed - speed at which the ground is moving
	* 
	*/
	void move(float speed);

	/**
	* The draw function simply draws the corresponding sprite of the entity to the screen at a given position.
	* 
	*/
virtual	void draw();

	/**
	* Accessor to return the vector of type Sprite
	* 
	*/
	const vector<sf::Sprite> &getSprites() const;

protected:
	GameDataRef localGameData;
	vector<sf::Sprite> sprites;
	int _landHeight;	// don't think we need this
};