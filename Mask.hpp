/**
* @version 1.1
* @date    22-Nov-2020
*
* Edited by Aidan Sheedy
* - Added deleteMask(int indice) function
* 
* @version 1.0
* @date    12-Nov-2020
*
* Created by Alessandro Menchetti
*/

#pragma once

#include "FloatingEntity.hpp"

using namespace std;

/**
* The Mask class extends FloatingEntity and has its own constructor as well as a defined spawn() function which was a pure virtual function in the base class Entity.
* 
*/
class Mask : public FloatingEntity {

	using FloatingEntity::FloatingEntity;

public:
	/**
	* The spawn() function simply spawns in a Mask object on the far right of the screen and uses the "Mask" sprite defined in the DEFINITIONS.hpp file.
	*
	*/
	void spawn();

	// initializes assets
	void init();

	void deleteMask(int indice);

private:
	// private sprite and texture attributes - these are used to spawn the next sprite
	sf::Sprite sprite;
	sf::Texture maskTexture;
};
