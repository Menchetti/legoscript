/**
* @version 1.0
* @date    22-Nov-2020
*
* Edited by Aidan Sheedy
* - Added constructor, updated some names
* 
* @version 1.0
* @date    12-Nov-2020
*
* Created by Alessandro Menchetti
*/

#pragma once

#include "Entity.hpp"

using namespace std;

/**
* The FloatingEntity class extends the Entity class and simply has an added method RandomizeYOffset() as well as an added variable _floatingYOffset to tell the game at what Y height to spawn in the Floating Entity.
* 
*/
class FloatingEntity : public Entity {

	using Entity::Entity;

public:
	/**
	* The RandomizeYOffset() method simply generates a random number between 1 and the specified maximum height for a floating entity to spawn at
	* 
	*/
	void randomizeYOffset();

protected:
	int floatingYOffset;
};
