/*
*
* Created by: Alex Koch
*/

#pragma once

#include <SFML/Graphics.hpp>
#include "GameOverState.hpp"
#include "Game.hpp"

using namespace std;

    class GameOverState : public State
    {
    public:
        GameOverState(GameDataRef gameData, int score, int mask);
        
        void init();
        void manageInput();
        void update();
        void draw();
        
    private:
        GameDataRef localGameData;
        
        sf::Sprite gameOverScreen;
        sf::Sprite backButton;
        sf::Sprite playButton;
        
        sf::Text scoreText;
        sf::Text maskText;

        int score;
        int mask;
    };











