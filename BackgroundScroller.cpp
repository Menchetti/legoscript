/**
* @ Version 1.0
* @ Date 15-Nov-2020
*
* Created by Chloe Talman
**/

#include "BackgroundScroller.hpp"
#include "GameState.hpp"
#include "math.h"

using namespace std;

BackgroundScroller::BackgroundScroller() : speed(MOVEMENT_SPEED) {
}

int BackgroundScroller::getSpeed() {
	return speed;
}

void BackgroundScroller::updateSpeed() {

	if (speed < 50){
		speed = 50;
	}
	else {
	//	speed = formula4(ScoreTracker.getScore());
	}
}

int BackgroundScroller::formula1(int score) {
	speed = score / 2;
	return speed;
}

int BackgroundScroller::formula2(int score) {
	speed = log(score);
	return speed;


}

int BackgroundScroller::formula3(int score) {
	score = pow(speed, 2);
	return speed;
}

int BackgroundScroller::formula4(int score) {
	speed += formula1(score);
	return speed;
}