/**
* @version 1.1
* @date    27-Nov-2020
*
* Edited by Aidan Sheedy
* - Changed gameOver flag to gameCondition flag
* 
* @version 1.0
* @date    14-Nov-2020
*
* Created by Aidan Sheedy
*/

#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "State.hpp"
#include "Game.hpp"
#include "CollisionDetector.hpp"
#include "ScoreTracker.hpp"
#include "BackgroundScroller.hpp"
#include "EntityGenerator.hpp"
#include "HUD.hpp"
#include "PhysicsEngine.hpp"
#include "Mask.hpp"
#include "Virus.hpp"
#include "Group.hpp"
#include "Ground.hpp"
#include "Player.hpp"

/**
 * GameState Class
 *
 * The GameState is the state that runs while the player is playing the game. Controls all game logic including updating
 * every frame, and drawing the frame to the window.
 */
class GameState : public State {

public:
	/**
	 * GameState Constructor
	 *
	 * Saves the gameData reference to be accessed by functions within the class
	 *
	 * @param gameData - Global game data passed to the constructor to store and reference when needed as a local
	 * 					 parameter.
	 */
	GameState(GameDataRef gameData);

	/**
	 * Loads all unloaded assets through the asset manager, instantiates all entity classes and necessary managers.
	 */
	void init();

	/**
	 * Checks listeners for all applicable keyboard and mouse inputs and applies the appropriate actions. Inputs are:
	 * - window closed 			-> close the window
	 * - up arrow pressed 		-> jump (for now there are no checks for double jumps but that will be necessary at some point)
	 * - down arrow pressed		-> duck
	 * - down arrow released	-> stop ducking
	 * - escape key pressed		-> pause the game
	 */
	void manageInput();

	/**
	 * Updates all entities in the following order:
	 * - Resets any frame-by-frame variables
	 * - Updates background scroller speed and the player animation
	 * - Move all entities and the background
	 * - spawn any new entities
	 * - update player and player location
	 * - check for collisions and take appropriate action (collect mask, end game with collisions with obstacles)
	 * - update the score
	 * - update the hub
	 * - if the game is over, replace with gameOverState
	 */
	void update();

	/**
	 * Clears the window, draws all the updated entities, HUD, etc. and displays the window.
	 */
	void draw();

	/**
	 * TO-DO - These functions will pause and play the game respectively, but I'm not entirely sure what that entails.
	 * 		   It might involve something with clocks and score, but for now they're empyt.
	 */
	void pause();
	void play();



private:
	GameDataRef localGameData;
	sf::Clock clock;

	sf::Sprite background;

	/**
	 * Manager/controller parameters
	 */
    CollisionDetector collisionDetector;
    ScoreTracker scoreTracker;
    BackgroundScroller backgroundScroller;
    EntityGenerator entityGenerator;
    PhysicsEngine physicsEngine;
    HUD hud;

    /**
     * Entity parameters
     */
	Mask* mask;
	Virus* virus;
	Group* group;
	Ground* ground;
	Player* player;

	int gameCondition;

	float timeElapsed;
};