/**
* @version 1.1
* @date    21-Nov-2020
*
* Edited by Aidan Sheedy
* - Removed unnecessary dt parameter
* 
* @version 1.0
* @date    29-Oct-2020
*
* Created by Alessandro Menchetti
*/

#pragma once

using namespace std;

class State {
public:
	virtual void init() = 0;
	
	virtual void manageInput() = 0;
	virtual void update() = 0;
	virtual void draw() = 0;
	
	virtual void pause() { }
	virtual void play() { }
};
