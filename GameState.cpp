/**
* @version 1.1
* @date	   27 - Nov - 2020
*
* Edited by Aidan Sheedy
* - Removed use of background texture
* - Implemented use of entity init() functions
* - Implemented GAME_STATE flags to wait for user input to start the game, and 
*   to flag game over when an obstacle is hit - hit "Space" to start
* - Added use of the player CollisionSprite for collision detection
* - Changed background colour to grey to increase visibility of entities
* - Fixed other minor bugs
* - Cleaned up commenting
*
* @version 1.0
* @date    14-Nov-2020
*
* Created by Aidan Sheedy
*/

#include <vector>

#include "GameState.hpp"
#include "DEFINITIONS.hpp"
#include "PhysicsEngine.hpp"
#include "GameOverState.hpp"
#include "PauseState.hpp"

#include <iostream>

using namespace std;

GameState::GameState(GameDataRef gameData) : localGameData(gameData) {
}

void GameState::init() {
    // TO-DO: Load in Sound Assets

    // instantiate entity classes and other necessary classes
    mask = new Mask(localGameData);
    virus = new Virus(localGameData);
    group = new Group(localGameData);
    ground = new Ground(localGameData);
    player = new Player(localGameData);

    hud = HUD(localGameData);

    mask->init();
    virus->init();
    group->init();

    // initialize the score tracker (sets score to 0, etc.) and updates the HUD
    scoreTracker.init();
    hud.update(scoreTracker.getScore(), scoreTracker.getNumMasks(), 0);

    gameCondition = GAME_STATE_WAITING;
} // end init()

void GameState::manageInput() {

    sf::Event event;
    // Check for closed window and for key inputs
    while (this->localGameData->window.pollEvent(event)) {
        if (gameCondition != GAME_STATE_OVER) {
            if (sf::Event::Closed == event.type)
                this->localGameData->window.close();

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
                player->jump();
                if (gameCondition == GAME_STATE_WAITING) {
                    gameCondition = GAME_STATE_PLAYING;
                    clock.restart();
                }
                // TO-DO - we might need to process a check to see if the player jumping already, but that might be better
                //         to do in the actual jump function.
            } // end if

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
                player->duck();
                if (gameCondition == GAME_STATE_WAITING) {
                    gameCondition = GAME_STATE_PLAYING;
                    clock.restart();
                }
            }
            else if (sf::Event::KeyReleased) {
                if (event.key.code == sf::Keyboard::Down)
                    player->unDuck();
            } // end else if

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
                this->localGameData->stateManager.addState(stateRef(new PauseState(localGameData)), false);  // pause when the escape key is pressed
                gameCondition = GAME_STATE_WAITING;
            } // end if

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
                gameCondition = GAME_STATE_PLAYING;
            } // end if
        } // end if
    } // end while
} // end manageInput()

void GameState::update() {
    if (gameCondition == GAME_STATE_PLAYING) {
        int newMasks = 0;
        vector<int> masksToDelete{};
        timeElapsed = clock.getElapsedTime().asSeconds();

        // update background scroller
        backgroundScroller.updateSpeed();

        // animate player and move land
        float displacement = physicsEngine.getPlayerDisplacement(player);
        player->move(displacement);
        player->getSprite();
        player->update();
        ground->moveGround(backgroundScroller.getSpeed());

        // move entities - move function also deletes any entities that are off screen
        mask->move(backgroundScroller.getSpeed());
        virus->move(backgroundScroller.getSpeed());
        group->move(backgroundScroller.getSpeed());
        
        // spawn entities
        entityGenerator.spawnEntities(mask, virus, group);

        // check for mask collisions - delete any that collide
        vector <sf::Sprite> maskSprites = mask->getSprites();
        for (int i = 0; i < maskSprites.size(); i++) {
            if (collisionDetector.checkSpriteCollision(player->getCollisionSprite(), maskSprites.at(i))) {
                newMasks++;
                masksToDelete.push_back(i);
            } // end if
        } // end for
        for (int indice : masksToDelete) {
            mask->deleteMask(indice);
        } // end for each

        // check for obstacle collisions - game over
        vector <sf::Sprite> virusSprites = virus->getSprites();
        for (sf::Sprite sprite : virusSprites) {
            if (collisionDetector.checkSpriteCollision(player->getCollisionSprite(), sprite)) {
                gameCondition = GAME_STATE_OVER;
                clock.restart();
            } // end if
        } // end for
        vector <sf::Sprite> groupSprites = group->getSprites();
        for (sf::Sprite sprite : groupSprites) {
            if (collisionDetector.checkSpriteCollision(player->getCollisionSprite(), sprite)) {
                gameCondition = GAME_STATE_OVER;
                clock.restart();
            } // end if
        } // end for

        // update score
        scoreTracker.update(timeElapsed, newMasks);

        // update HUD
        hud.update(scoreTracker.getScore(), scoreTracker.getNumMasks(), timeElapsed);
    }
    // game over check
    if (gameCondition == GAME_STATE_OVER) {
        if (clock.getElapsedTime().asSeconds() > GAME_OVER_STATE_DISPLAY_DELAY) {
            this->localGameData->stateManager.addState(stateRef(new GameOverState(localGameData, scoreTracker.getScore(), scoreTracker.getNumMasks())), true);
        }
    }
}

void GameState::draw() {
    this->localGameData->window.clear(sf::Color(65, 148, 247)); // TO-DO - Colour is not fixed yet

    // draw entities, land, and player
    ground->drawGround();
    group->draw();
    virus->draw();
    mask->draw();
    player->draw();
    
    // draw hud
    hud.draw();
    if (gameCondition == GAME_STATE_WAITING) {
        hud.displayWaitText();
    }

    this->localGameData->window.display();
}

void GameState::pause() {
    cout << "Pausing" << endl;
    entityGenerator.pause();
}

void GameState::play() {
    cout << "Playing" << endl;
    entityGenerator.play();
    gameCondition = GAME_STATE_PLAYING;
}