/**
* @version 1.2
* @date    27-Nov-2020
*
* Edited by Aidan Sheedy
* - Fixed spawning positioning
* - Implemented use of Isometric movement
* 
* @version 1.1
* @date    22-Nov-2020
*
* Edited by Aidan Sheedy
* - Removed constructor, implemented the init() finction
* - changed some names to reflect class diagram
* 
* Created by Alessandro Menchetti
*/  
#include "Virus.hpp"
#include "DEFINITIONS.hpp"

using namespace std;

void Virus::spawn() {
	if (rand() % 2 == 1) {
		sprite.setTexture(localGameData->assetManager.getTexture("Green Virus Texture"));
	}
	else {
		sprite.setTexture(localGameData->assetManager.getTexture("Virus Texture"));
	}
	sprite.setPosition(916 + floatingYOffset * ISOMETRIC_RATIO, 794 - floatingYOffset * (1 - ISOMETRIC_RATIO) - sprite.getTexture()->getSize().y);
	sprites.push_back(sprite);
}

void Virus::init() {
	// load in assets
	localGameData->assetManager.loadTexture("Virus Texture", VIRUS_FILEPATH);
	localGameData->assetManager.loadTexture("Green Virus Texture", GREEN_VIRUS_FILEPATH);
	virusTexture = localGameData->assetManager.getTexture("Virus Texture");
	sprite.setTexture(virusTexture);
}