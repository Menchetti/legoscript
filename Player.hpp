/**
* @version 1.2
* @date    27-Nov-2020
*
* Edited by Aidan Sheedy
* - Added collision sprite (hitbox) parameter
* 
 * @Version 1.1
 * @Date 14-Nov-2020
 *
 * Edited by Aidan Sheedy
 * - Restructured to group like attributes and functions
 * - moved animate() to private
 * - added update() function
 * - added texture parameters for ducking, jumping, and standing still
 * 
 * Created by Alex Koch
 **/

#pragma once

#include <SFML/Graphics.hpp>

#include "DEFINITIONS.hpp"
#include "Game.hpp"

#include <vector>

using namespace std;

	class Player
	{
	private:
		GameDataRef localGameData; //game data passed by state machine 
		sf::Clock clock; //local clock used by the player to determine jump times, etc

		sf::Sprite playerSprite; //the player asset
		vector<sf::Texture> animationFrames; //the vector used to animate the player - contains each animation frame
		unsigned int animationIterator; //token used to implement animation vector
		sf::Texture duckingTexture;
		sf::Texture jumpingTexture;
		sf::Texture stillTexture;

		sf::Sprite playerHitbox;

		int playerState; // used to log which state the player is in
		float velocity; // used to determine the player's vertical velocity

		void animate(); // animate the player via the use of the vector

	public:
		Player(GameDataRef gameData); //constructor
		~Player(); //destructor

		void draw(); //render the player
		void update(); // udpates the players texture based on the current state
		void jump(); //increase vertcial veloctiy and change the state
		void duck(); //change the asset state
		void unDuck(); //revert asset state back to running
		void move(float displacement); //moves the player by the provided displacement 

		const sf::Sprite &getSprite() const; //retrieve current state
		const sf::Sprite& getCollisionSprite() const; // retrieve the sprite used for collision detection

		void setVelocity(float vel); //sets  the player velocity
		float getVelocity(); //returns the velocity 

		bool isDucking(); //returns a true or false value if the player is in the ducking state or not 
    };
