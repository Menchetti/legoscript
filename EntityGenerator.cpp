/**
* @version 1.1
* @date	   27-Nov-2020
*
*Edited by Aidan Sheedy
* - Fixed some variable names to align with UML Class Diagram
* - Made entities pointers
* - Made test variable a parameter to allow for class-wide scope
*
*/

#include "EntityGenerator.hpp"

using namespace std;

EntityGenerator::EntityGenerator() {
	clock.restart();
}

void EntityGenerator::spawnEntities(Mask* mask, Virus* virus, Group* group) {
	currentTime = clock.getElapsedTime().asSeconds() + pauseTime;
	test = rand() % 5;
	if (currentTime > 2) {
		switch (test) {
		case 0:
			mask->randomizeYOffset();
			mask->spawn();
			break;
		case 1:
		case 2:
			virus->randomizeYOffset();
			virus->spawn();
			break;
		case 3:
		case 4:
			group->randomizeSize();
			group->spawn();
			break;
		}
		clock.restart();
		pauseTime = 0;
		currentTime = 0;
	}
}

void EntityGenerator::pause() {
	pauseTime = clock.getElapsedTime().asSeconds();
}

void EntityGenerator::play() {
	clock.restart();
	currentTime = pauseTime;
}