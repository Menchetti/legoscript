/**
* @version 1.1
* @date	   27-Nov-2020
*
* Edited by Aidan Sheedy
* - removed unneccesary else
*/
#include "CollisionDetector.hpp"


CollisionDetector::CollisionDetector(){

}
bool CollisionDetector::checkSpriteCollision(sf::Sprite sprite1, sf::Sprite sprite2){
    sf::Rect<float> rect1 = sprite1.getGlobalBounds();
    sf::Rect<float> rect2 = sprite2.getGlobalBounds();
    if (rect1.intersects(rect2)) {
        return true;
    }
     return false;
}
bool CollisionDetector::checkSpriteCollision(sf::Sprite sprite1, float scale1, sf::Sprite sprite2, float scale2){
    sprite1.setScale(scale1, scale2);
    sprite2.setScale(scale1, scale2);
    sf::Rect<float> rect1 = sprite1.getGlobalBounds();
    sf::Rect<float> rect2 = sprite2.getGlobalBounds();
    if(rect1.intersects(rect2)){
        return true;
    }else{
        return false;
    }
}
    


