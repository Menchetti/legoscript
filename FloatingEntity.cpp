/**
* @version 1.2
* @date	   27 - Nov - 2020
*
*Edited by Aidan Sheedy
* - Changed implementation of randomizeYOfsset() to take use FLOATING_Y_VARIABILITY definition
*
* @version 1.1
* @date    12 - Nov - 2020
* 
* Edited by Aidan Sheedy
* - changed some names, added use of definitions file for GROUND_HEIGHT
*
* Created by Alessandro Menchetti
*/

#include "FloatingEntity.hpp"
#include "DEFINITIONS.hpp"

using namespace std;

void FloatingEntity::randomizeYOffset() { //generate random number between 1 and _landheight
	floatingYOffset = rand() % (FLOATING_Y_VARIABILITY);
}