/**
* @version 1.0
* @date    22-Nov-2020
*
* Created by Alessandro Menchetti
*/

#pragma once

#include <SFML/Graphics.hpp>
#include "State.hpp"
#include "Game.hpp"

using namespace std;

class LaunchState : public State {
public:
	LaunchState(GameDataRef gameData);

	void init();
	void manageInput();
	void update();
	void draw();

private:
	GameDataRef localGameData;

	sf::Clock localClock;

	sf::Sprite splashBackground;
};