#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "State.hpp"
#include "Game.hpp"


class PauseState : public State {
public:
    PauseState(GameDataRef gameData);
    
    void init();
    
    void manageInput();
    void update();
    void draw();
private:
    GameDataRef localGameData;
    
    sf::Sprite background;
    sf::Sprite title;
    sf::Sprite settingsButton;
    sf::Sprite quitButton;
    sf::Sprite instructionsButton;
    sf::Sprite playButton;
    //sf::Sprite returnToGameButton;
    
    sf::SoundBuffer pauseMusicBuffer;
    sf::SoundBuffer buttonPressBuffer;
    sf::Sound buttonPressSound;
    sf::Sound pauseMusicSound;
    
};
