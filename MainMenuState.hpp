/**
* @version 1.0
* @date    22-Nov-2020
*
* Created by Alessandro Menchetti
*/

#pragma once

#include <SFML/Graphics.hpp>
#include "State.hpp"
#include "Game.hpp"

using namespace std;

class MainMenuState : public State {
public:
	MainMenuState(GameDataRef gameData);

	void init();
	void manageInput();
	void update();
	void draw();

private:
	GameDataRef localGameData;

	sf::Sprite background;
	//sf::Sprite title;
	sf::Sprite playButton;
	sf::Sprite helpButton;
	sf::Sprite quitButton;
};