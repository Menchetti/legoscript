/*
* @version 1.1
* @date    27-Nov-2020
*
* Edited by Aidan Sheedy
* - Added definitions include
* - Removed unnecessary parameter in constructor
* - Fixed bug in update by properly using currentTime parameter
* - added use of SCORE_FILE_PATH definition
* 
* @version 1.0
* @date	 25-nov-2020
*
* Created by: Chloe Talman
*/

#include "ScoreTracker.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include "DEFINITIONS.hpp"

using namespace std;

ScoreTracker::ScoreTracker() {}

void ScoreTracker::init() { //at beginning of game set everything to zero
	score = 0; 
	numMasks = 0;
	update(0, numMasks); 
}

void ScoreTracker::update(float currentTime, int newMasks) { //calculates time with inputted data
	int elapsedTime = currentTime - previousTime;
	numMasks += newMasks;
	score += elapsedTime + 10 * newMasks;
	previousTime = currentTime;
}

void ScoreTracker::saveScore(string userName) {
	string scoreString;
	string user;
	ifstream fileIn(SCORE_FILE_PATH);
	while (!fileIn.eof()) { //reads all scores and usernames from file
		fileIn >> scoreString;
		scores.push_back(scoreString);
		fileIn >> user;
		userNames.push_back(user); //this is based on a file layout of score followed by username
	}

	int greaterThan = 10; 

	for (int i = 9; i >= 0; i--) { //checks to see if current score is greater than each score in the file, checking from lowest to highest high score
		string str = scores[i];
		if (score > stoi(str)) {
			greaterThan = i;
		}
		else {
			break; //could leave out else because break isn't nice coding, however if it isn't greater than 
					//the ith score, it won't be greater than the i-1th score
		}
	}

	fileIn.close();

	if (greaterThan < 10) { //finds position of score if it's greater than the lowest score and updates the file to include it
		for (int i = 9; i >= greaterThan; i--) { //puts the score in the correct position, moves all scores below it down
			if (i == greaterThan){
				scores[i] = score;
				userNames[i] = userName;
			}
			else {
				scores[i] = scores[i - 1];
				userNames[i] = userNames[i - 1];
			}
		}

		ofstream fileOut(SCORE_FILE_PATH);
		fileOut.open(" ", ios::out | ios::trunc); //sets it so the file clears before I write to it


		for (int i = 0; i < 10; i++) {
			fileOut << scores[i] << " " << userNames[i] << endl;
		}
		fileOut.close();
	}
	//if it isn't a top 10 score the file won't be updated
	
}

int ScoreTracker::getScore() { //score accessor
	return score;
}

int ScoreTracker::getNumMasks() { //numMasks accessor
	return numMasks;
}

