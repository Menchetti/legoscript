/**
* @version 1.2
* @date    27-Nov-2020
*
* Edited by Aidan Sheedy
* - Fixed spawning positioning
* - Implemented use of Isometric movement
* 
* @version 1.1
* @date    21-Nov-2020
* 
* Edited by Aidan Sheedy
* - Implemented init()
* - Changed parameter names to match .hpp
* - adjusted randomizeSize() to only set the size to between 1 and 3.
*
* Created by Alessandro Menchetti
*/

#include "Group.hpp"
#include "DEFINITIONS.hpp"

using namespace std;

void Group::init() {
	// load and get textures
	this->localGameData->assetManager.loadTexture("Small Group Texture", SMALL_GROUP_FILEPATH);
	this->localGameData->assetManager.loadTexture("Medium Group Texture", MEDIUM_GROUP_FILEPATH);
	this->localGameData->assetManager.loadTexture("Large Group Texture", LARGE_GROUP_FILEPATH);
	smallGroupTexture = this->localGameData->assetManager.getTexture("Small Group Texture");
	mediumGroupTexture = this->localGameData->assetManager.getTexture("Medium Group Texture");
	largeGroupTexture = this->localGameData->assetManager.getTexture("Large Group Texture");

	// default size is 1
	groupSize = 1;
	spriteToSpawn.setTexture(smallGroupTexture);
}

void Group::spawn() {
	if (groupSize == 1) { //example with 3 different size groups
		spriteToSpawn.setTexture(smallGroupTexture);
	}
	else if (groupSize == 2) {
		spriteToSpawn.setTexture(mediumGroupTexture);
	}
	else if (groupSize == 3) {
		spriteToSpawn.setTexture(largeGroupTexture);
	}
	spriteToSpawn.setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(spriteToSpawn.getTexture()->getSize())));
	spriteToSpawn.setPosition(790 + 126, this->localGameData->window.getSize().y - spriteToSpawn.getTexture()->getSize().y + 74);
	sprites.push_back(spriteToSpawn);
}

void Group::randomizeSize() {
	groupSize = rand() % (NUM_GROUP_SIZES)+1;
}