/**
* @version 1.2
* @date	   27-Nov-2020
* 
* Edited by Aidan Sheedy
* - Incorporated isometric movement
* - Added testing section to display hitboxes when needed
* 
* @version 1.1
* @date    21-Nov-2020
* 
* Edite by Aidan Sheedy
* - Changed parameter names to match .hpp
* - modified parameter for move() to include speed of the ground
*
* Created by Alessandro Menchetti
*/

#include "Entity.hpp"
#include "DEFINITIONS.hpp"

using namespace std;

Entity::Entity(GameDataRef data) : localGameData(data) {}

void Entity::move(float speed) {
	for (int i = 0; i < sprites.size(); i++) {
		if (sprites.at(i).getPosition().x < 0 - sprites.at(i).getLocalBounds().width) { //if sprite is offscreen remove it from the vector to save resources
			sprites.erase(sprites.begin() + i);
		}
		else {
			sf::Vector2f position = sprites.at(i).getPosition(); //if not then move it
			float movement = speed * TICK_RATE; //MOVEMENT_SPEED and TICK_RATE would be a definition in the DEFINITIONS.hpp file
			sprites.at(i).move(-movement * ISOMETRIC_RATIO, -movement * (1 - ISOMETRIC_RATIO));
		}
	}
}

void Entity::draw() {
	for (unsigned short int i = 0; i < sprites.size(); i++) {
		this->localGameData->window.draw(sprites.at(i));
#ifdef SHOW_HITBOXES
		sf::FloatRect hitbox = sprites.at(i).getGlobalBounds();
		sf::RectangleShape textureRect(sf::Vector2f(hitbox.width, hitbox.height));
		textureRect.setOutlineThickness(2);
		textureRect.setOutlineColor(sf::Color::Black);
		textureRect.setFillColor(sf::Color::Transparent);
		textureRect.setPosition(sprites.at(i).getPosition());
		this->localGameData->window.draw(textureRect);
#endif
	}
}

const vector<sf::Sprite>& Entity::getSprites() const {
	return sprites;
}