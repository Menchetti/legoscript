/*
*
* Created by: Alex Koch
*/


#pragma once

#include <sstream>
#include "DEFINITIONS.hpp"
#include "HelpState.hpp"
#include "GameState.hpp"

#include <iostream>

using namespace std;
	HelpState::HelpState(GameDataRef gameData) : localGameData(gameData)
	{

	}

	void HelpState::init()
	{
		this->localGameData->assetManager.loadTexture("Help Menu Screen", HELP_MENU_SCREEN_FILEPATH); 
		this->localGameData->assetManager.loadTexture("Back Button", BACK_BUTTON_FILEPATH);

		helpMenuScreen.setTexture(this->localGameData->assetManager.getTexture("Help Menu Screen"));
		backButton.setTexture(this->localGameData->assetManager.getTexture("Back Button"));

		//should be exact button placement based off pixel placement in photoshop
		backButton.setPosition(static_cast<int>((WINDOW_WIDTH / 2) - (backButton.getGlobalBounds().width / 2) - 15), 180);
	}

	void HelpState::manageInput()
	{
		sf::Event event;

        //while there is an event
		while (this->localGameData->window.pollEvent(event)) 
		{
			if (sf::Event::Closed == event.type) 
			{
                //close the window if we hit X
				this->localGameData->window.close();
			}

			if (this->localGameData->inputManager.isSpriteClicked(backButton, sf::Mouse::Left, this->localGameData->window))
			{
				// Back to previous state
				this->localGameData->stateManager.removeState();
			}
		}
	}

	void HelpState::update()
	{
		
	}

	void HelpState::draw()
	{
        //draws all of the asssetManager onto the window
		this->localGameData->window.clear(sf::Color::Black);

		this->localGameData->window.draw(this->helpMenuScreen);
		this->localGameData->window.draw(this->backButton);

		this->localGameData->window.display();
	}

    void HelpState::play()
	{
		
	}

    void HelpState::pause()
	{
		
	}