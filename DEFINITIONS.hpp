/**
* @version 1.3
* @date	   27-Nov-2020
* 
* Edited by Aidan Sheedy
* - added some file paths
* - Reduced some variability definitions to just 1
* - modified some values
* - Added GAME_STATE definitions
* - added a debugging definition
* 
* @version 1.2
* @date	   21-Nov-2020
* 
* Edited by Aidan Sheedy
* - added some file paths
* 
* @version 1.1
* @date	   18-Nov-2020
* 
* Edited by Aidan Sheedy
* - added some temporary definition assignments for testing purposes
* - added some more environmental and spawn related definitions
* 
* @version 1.0
* @date    14-Nov-2020
*
* Created by Aidan Sheedy
*/

#pragma once

// Display related definitions
#define WINDOW_HEIGHT 720
#define WINDOW_WIDTH 1290

// Frame rate related definitions
#define TICK_RATE 1.0/90.0

// Screen timing related definitions
#define SPLASH_SCREEN_SHOW_TIME 2
#define BACK_STORY_SHOW_TIME
#define GAME_OVER_STATE_DISPLAY_DELAY 1

/** Filepath related definitions - should be in the form:
 *
 *  #define <ASSET_NAME>_FILEPATH "Resources/subfolder/<filename>"
 *
 * Where the possible subfolders are:
 * - textures
 * - audio
 * -> note, more may be added later if required
 *
 * Texture sheet example provided
 */
#define SPLASH_BACKGROUND_FILEPATH "Resources/Assets/launchScreen.png"
#define MAIN_MENU_BG_FILEPATH "Resources/Assets/MainScreen.png"
#define HELP_MENU_SCREEN_FILEPATH "Resources/Assets/HelpScreen.png"
#define PAUSE_MENU_BACKGROUND_FILEPATH "Resources/Assets/PausedScreen.png"
#define GAME_OVER_SCREEN_FILEPATH "Resources/Assets/GameOverScreen.png"

#define BACK_BUTTON_FILEPATH "Resources/Assets/BackButton.png"
#define SETTINGS_BUTTON_FILE_PATH "Resources/Assets/SettingsIcon.png"
#define INSTRUCTIONS_BUTTON_FILE_PATH "Resources/Assets/HelpButton.png"
#define PLAY_BUTTON_FILEPATH "Resources/Assets/PlayButton.png"
#define HELP_BUTTON_FILEPATH "Resources/Assets/HelpButton.png"
#define QUIT_BUTTON_FILEPATH "Resources/Assets/QuitButton.png"

#define PLAYER_DUCKING_FILEPATH "Resources/Assets/Slide.png"
#define PLAYER_STILL_FILEPATH "Resources/Assets/Stand.png"
#define PLAYER_JUMPING_FILEPATH "Resources/Assets/Stand2.png"

#define PLAYER_FRAME_1_FILEPATH "Resources/Assets/Walk1.png"
#define PLAYER_FRAME_2_FILEPATH "Resources/Assets/Walk2.png"
#define PLAYER_FRAME_3_FILEPATH "Resources/Assets/Walk1.png"
#define PLAYER_FRAME_4_FILEPATH "Resources/Assets/Walk2.png"

#define GROUND_1_FILEPATH "Resources/Assets/BuildingNormal1.png"

#define SMALL_GROUP_FILEPATH "Resources/Assets/singleZombie.png"
#define MEDIUM_GROUP_FILEPATH "Resources/Assets/singleZombie.png"
#define LARGE_GROUP_FILEPATH "Resources/Assets/Zombie.png"

#define MASK_FILEPATH "Resources/Assets/Mask_1.png"
#define VIRUS_FILEPATH "Resources/Assets/Virus.png"
#define GREEN_VIRUS_FILEPATH "Resources/Assets/GreenVirus.png"

#define SCORE_FILE_PATH "Resources/Highscore.txt"

#define FONT_FILE_PATH "Resources/Fonts/Arial.ttf"


 /** Spawning related definitions
  * -> note, these are mostly related to adding randomness to the functionality of the game,
  *          and so they will likely be more useful towards the end of the development cycle.
  *          More or less may be required as we go.
  */
#define OBSTACLE_SPAWN_FREQUENCY
#define MASK_SPAWN_FREQUENCY
#define OBSTACLE_SPAWN_VARIABILITY
#define MASK_SPAWN_VARIABILITY
#define FLOATING_Y_VARIABILITY 300
#define NUM_GROUP_SIZES 3

  // Player related definitions:
#define PLAYER_ANIMATION_DURATION 0.4f
#define PLAYER_JUMP_VELOCITY -800
#define PLAYER_STATE_STILL		1
#define PLAYER_STATE_RUNNING	2
#define PLAYER_STATE_JUMPING	3
#define PLAYER_STATE_DUCKING	4

// GameState Definitions:
#define GAME_STATE_WAITING 0
#define GAME_STATE_PLAYING 1
#define GAME_STATE_OVER 2


// Environment related definitions
#define GRAVITY 1200

#define GROUND_HEIGHT 350
#define GROUND_X_SPAWN 1626
#define GROUND_Y_SPAWN 1170

#define MOVEMENT_SPEED 400 // Temporary for now I believe
#define ISOMETRIC_RATIO 0.6335

#define MOVEMENT_SPEED 500 // Temporary for now I believe
#define ISOMETRIC_RATIO 0.6335

// For debugging, un-comment to show hitboxes
//#define SHOW_HITBOXES