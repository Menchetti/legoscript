/*
* @version 1.0
* @date	 25-nov-2020
*
* Created by: Chloe Talman
*/

#include "HUD.hpp"
#include <string>
#include <iostream>

using namespace std;

HUD::HUD() {}

HUD::HUD(GameDataRef localGameData) : localGameData(localGameData) { //writes the score, time and number of masks to the screen

	localGameData->assetManager.loadFont("Font", FONT_FILE_PATH);

	waitText.setFont(localGameData->assetManager.getFont("Font"));
	waitText.setString("Press Space to Begin.");
	waitText.setCharacterSize(72);
	waitText.setFillColor(sf::Color::White);
	waitText.setStyle(sf::Text::Bold);
	waitText.setOrigin(waitText.getGlobalBounds().width / 2, waitText.getGlobalBounds().height);
	waitText.setPosition(840, 250);
	
	scoreText.setFont(localGameData->assetManager.getFont("Font"));
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(32);
	scoreText.setFillColor(sf::Color::White); 
	scoreText.setStyle(sf::Text::Bold);
	scoreText.setPosition(560, 25);


	timeText.setFont(this->localGameData->assetManager.getFont("Font"));
	timeText.setString("Time elapsed: 0");
	timeText.setCharacterSize(32);
	timeText.setFillColor(sf::Color::White);
	timeText.setStyle(sf::Text::Bold);
	timeText.setPosition(943, 25);

	maskText.setFont(this->localGameData->assetManager.getFont("Font"));
	maskText.setString("# Masks: 0"); 
	maskText.setCharacterSize(32);
	maskText.setFillColor(sf::Color::White);
	maskText.setStyle(sf::Text::Bold);
	maskText.setPosition(750, 25);
	
}

HUD::~HUD() {
}

void HUD::draw() {
	localGameData->window.draw(scoreText);
	localGameData->window.draw(timeText);
	localGameData->window.draw(maskText);
}

void HUD::update(int score, int numMasks, float time) {
	int localScore = score;

	scoreText.setString("Score: " + to_string(score));
	timeText.setString("Time elapsed: " + to_string((int)time));
	maskText.setString("# Masks: " + to_string(numMasks));
}

void HUD::displayWaitText() {
	localGameData->window.draw(waitText);
}