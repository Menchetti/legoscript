/**
 * @Version 1.1
 * @Date 21-Nov-2020
 * 
 * Edited by Aidan Sheedy
 * - changed parameter for getPlayerDisplacement to pass a pointer as opposed to passing by reference.
 * 
 * @Version 1.0
 * @Date 14-Nov-2020
 *
 * Created by Aidan Sheedy
 **/

#pragma once

#include "Player.hpp"

class PhysicsEngine {
public:
	/**
	 * Empty constructor
	 */
	PhysicsEngine();

	/**
	 * This function finds the displacement of the player over one tick based on it's current velocity using Newtonian
	 * mechanics. It also updates the velocity of the player at the same time.
	 *
	 * @param player: the player instance to be updated
	 * @return the displacement of the player for one tick
	 */
	static float getPlayerDisplacement(Player* player);
};