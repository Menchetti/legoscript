/**
 * @Version 1.1
 * @Date 21-Nov-2020
 *
 * Edited by Aidan Sheedy
 * - updated the player paremeter passed to getPlayerDisplacement to be a pointer
 * - removed unnecessary comments
 * 
 * @Version 1.0
 * @Date 14-Nov-2020
 *
 * Created by Aidan Sheedy
 **/

#include "PhysicsEngine.hpp"
#include "Player.hpp"
#include "DEFINITIONS.hpp"

// enpty constructor
PhysicsEngine::PhysicsEngine() {
} // end constructor

float PhysicsEngine::getPlayerDisplacement(Player* player) {
	float newVelocity = 0.0;
	int yPos = 0;
	int displacement = 0;

	// get sprite so we can check it's position
	sf::Sprite playerSprite = player->getSprite();
	sf::Vector2f PlayerPos = playerSprite.getPosition();
	yPos = PlayerPos.y;

	// Check height, if position > ground then calculate velocity and displacement
	if (yPos <= GROUND_HEIGHT || player->getVelocity() < 0) {
		newVelocity = GRAVITY * TICK_RATE + player->getVelocity();
		displacement = newVelocity * TICK_RATE;
	} else {
		newVelocity = 0;
		displacement = 0;
	} // end if else

	player->setVelocity(newVelocity);
	return displacement;
} // end getPlayerDisplacement