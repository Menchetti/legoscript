#include <sstream>
#include "DEFINITIONS.hpp"
#include "MainMenuState.hpp"
#include "HelpState.hpp"
#include "GameState.hpp"

#include <iostream>

using namespace std;

MainMenuState::MainMenuState(GameDataRef data) : localGameData(data) { }

void MainMenuState::init() {
	
	//Loading Assets
	this->localGameData->assetManager.loadTexture("Main Menu Background", MAIN_MENU_BG_FILEPATH); //bg means background
	this->localGameData->assetManager.loadTexture("Play Button", PLAY_BUTTON_FILEPATH);
	this->localGameData->assetManager.loadTexture("Help Button", HELP_BUTTON_FILEPATH);
	this->localGameData->assetManager.loadTexture("Quit Button", QUIT_BUTTON_FILEPATH);

	//Initializing
	background.setTexture(this->localGameData->assetManager.getTexture("Main Menu Background"));
	playButton.setTexture(this->localGameData->assetManager.getTexture("Play Button"));
	helpButton.setTexture(this->localGameData->assetManager.getTexture("Help Button"));
	quitButton.setTexture(this->localGameData->assetManager.getTexture("Quit Button"));

	//Placing on screen
	background.setPosition(-10, 0);
	playButton.setPosition((WINDOW_WIDTH / 2) - (playButton.getGlobalBounds().width / 2) + 7, 440);
	helpButton.setPosition((WINDOW_WIDTH / 2) - helpButton.getGlobalBounds().width + 7 - 5, 560);
	quitButton.setPosition((WINDOW_WIDTH / 2) + 7 + 5, 560);
}

void MainMenuState::manageInput() {
	sf::Event event;

	while (this->localGameData->window.pollEvent(event)) {
		if (sf::Event::Closed == event.type) {
			this->localGameData->window.close();
		}

		if (this->localGameData->inputManager.isSpriteClicked(this->playButton, sf::Mouse::Left, this->localGameData->window)) {
			this->localGameData->stateManager.addState(stateRef(new GameState(localGameData)), true);
		}

		if (this->localGameData->inputManager.isSpriteClicked(this->helpButton, sf::Mouse::Left, this->localGameData->window)) {
			this->localGameData->stateManager.addState(stateRef(new HelpState(localGameData)), false); //should going to help screen replace MainMenuState on stack or just add HelpState to top of stack and pop when done? (isReplacing is also set to true in HelpState)
		}

		if (this->localGameData->inputManager.isSpriteClicked(this->quitButton, sf::Mouse::Left, this->localGameData->window)) {
			this->localGameData->window.close();
		}
	}
}

void MainMenuState::update() {

}

void MainMenuState::draw() {
	this->localGameData->window.clear(sf::Color::Black);

	this->localGameData->window.draw(this->background);
	this->localGameData->window.draw(this->playButton);
	this->localGameData->window.draw(this->helpButton);
	this->localGameData->window.draw(this->quitButton);

	this->localGameData->window.display();
}