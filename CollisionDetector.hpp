#pragma once

#include <SFML/Graphics.hpp>


class CollisionDetector{
public:
    CollisionDetector();
    bool checkSpriteCollision(sf::Sprite sprite1, sf::Sprite sprite2);
    bool checkSpriteCollision(sf::Sprite sprite1, float scale1, sf::Sprite sprite2, float scale2);
};
