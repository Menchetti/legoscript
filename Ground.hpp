#pragma once


using namespace std;

#include <SFML/Graphics.hpp>
#include "Game.hpp"
#include <vector>

class Ground
{
public:
	Ground(GameDataRef localGameData); //makes 2 ground sprites and sets their positions
	void moveGround(int speed);  //sets position of sprites, uses 2 ground sprites to ensure the ground is always being shown
									//moves sprites at speed given from background scroller
	void drawGround(); 
private:
	GameDataRef localGameData;
	std::vector<sf::Sprite>groundSprites; //the ground sprite vector

};

