/**
* @version 1.0
* @date    29-Oct-2020
*
* Created by Alessandro Menchetti
*/

#include "StateManager.hpp"

using namespace std;

void StateManager::addState(stateRef newState, bool isReplacing) { //Implementation of the addState method
	this->_isAdding = true;
	this->_isReplacing = isReplacing;

	this->_newState = move(newState);
}

void StateManager::removeState() { //Implementation of the removeState method
	this->_isRemoving = true;
}

void StateManager::processStateChanges() { //Implementation of the processStateChanges method
	if (this->_isRemoving && !this->_states.empty()) { //if a state is being removed and the stack is not already empty pop the top state
		this->_states.pop();

		if (!this->_states.empty()) { //if the stack is still not empty resume the new top state
			this->_states.top()->play();
		}

		this->_isRemoving = false;
	}

	if (this->_isAdding) { //if a state is being added and the stack is not empty
		if (!this->_states.empty()) {
			if (this->_isReplacing) { //if the added state is replacing the current one pop the current top of the stack
				this->_states.pop();
			}
			else { //else pause the top of the stack
				this->_states.top()->pause();
			}
		}

		this->_states.push(move(this->_newState)); //push the new state to the top of the stack
		this->_states.top()->init();
		this->_isAdding = false;
	}
}

stateRef& StateManager::getActiveState() { //Implementation of the getActiveState accessor
	return this->_states.top();
}
