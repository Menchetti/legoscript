/**
* @version 1.1
* @date    22-Nov-2020
* 
* Edited by Aidan Sheedy
* - Changed constructor to implement the FloatingEntity constructor
* 
* @version 1.0
* @date    12-Nov-2020
* 
* Created by Alessandro Menchetti
*/

#pragma once

#include "FloatingEntity.hpp"

using namespace std;

/**
* The Virus class extends FloatingEntity and has its own constructor as well as a defined spawn() function which was a pure virtual function in the base class Entity.
*
*/
class Virus : public FloatingEntity {

	using FloatingEntity::FloatingEntity;

public:
	/**
	* The spawn() function simply spawns in a Mask object on the far right of the screen and uses the "Virus" sprite defined in the DEFINITIONS.hpp file.
	*
	*/
	void spawn();

	// initializes private texture and sprite parameters
	void init();

private:
	// spawning texture and sprite, used to generate new sprites to spawn
	sf::Sprite sprite;
	sf::Texture virusTexture;
};
