#pragma once

#include "Game.hpp"
//#include "ScoreTracker.hpp"

using namespace std;

class BackgroundScroller{
public:
	BackgroundScroller();

	void updateSpeed(); //calculates the speed (currently using one of the formulas - but that can be moved into the function once we choose how we want to calculate speed)
	int getSpeed(); //calls update speed to get the current speed based on score

private:
	int formula1(int); //diff formulas for calculating speed
	int formula2(int);
	int formula3(int);
	int formula4(int);

	int speed;
	GameDataRef localGameData;
//	ScoreTracker scoreTracker;
};

