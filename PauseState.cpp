#pragma once

#include <sstream>
#include "DEFINITIONS.hpp"
#include "PauseState.hpp"
#include "GameState.hpp"
#include "MainMenuState.hpp"
#include "HelpState.hpp"

using namespace std;

PauseState::PauseState(GameDataRef gameData) : localGameData(gameData) {}

// Loading, intializing and placing all pause menu sprites
void PauseState::init() {
    
    //Loading Assets
    this->localGameData->assetManager.loadTexture("Pause Menu Background", PAUSE_MENU_BACKGROUND_FILEPATH);
    this->localGameData->assetManager.loadTexture("Quit Button", QUIT_BUTTON_FILEPATH);
    this->localGameData->assetManager.loadTexture("Instructions Button", INSTRUCTIONS_BUTTON_FILE_PATH);
    this->localGameData->assetManager.loadTexture("Play Button", PLAY_BUTTON_FILEPATH);
    
    // Initializing
    background.setTexture(this->localGameData->assetManager.getTexture("Pause Menu Background"));
    quitButton.setTexture(this->localGameData->assetManager.getTexture("Quit Button"));
    instructionsButton.setTexture(this->localGameData->assetManager.getTexture("Instructions Button"));
    playButton.setTexture(this->localGameData->assetManager.getTexture("Play Button"));
    
    // Placing - Positions will likely need to be played with
    instructionsButton.setPosition(static_cast<int>((WINDOW_WIDTH / 2) - (instructionsButton.getLocalBounds().width / 2)), 200);
    quitButton.setPosition(static_cast<int>((WINDOW_WIDTH) / 2 - (quitButton.getLocalBounds().width / 2)), 300);
    playButton.setPosition((WINDOW_WIDTH / 2) - (playButton.getLocalBounds().width / 2), 400);
}

void PauseState::manageInput() {
    sf::Event event;
    while(this->localGameData->window.pollEvent(event)) {
        if(sf::Event::Closed == event.type) {
            this->localGameData->window.close();
        }
        // Quit Game
        if (this->localGameData->inputManager.isSpriteClicked(this->quitButton, sf::Mouse::Left, this->localGameData->window)){
            this->localGameData->stateManager.addState(stateRef(new MainMenuState(this->localGameData)));
        }
        // Go to instructions menu
        if (this->localGameData->inputManager.isSpriteClicked(this->instructionsButton, sf::Mouse::Left, this->localGameData->window)){
            this->localGameData->stateManager.addState(stateRef(new HelpState(this->localGameData)), false);
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)){
            this->localGameData->stateManager.removeState();
        }
        if(this->localGameData->inputManager.isSpriteClicked(this->playButton, sf::Mouse::Left, this->localGameData->window)){
            this->localGameData->stateManager.removeState();
        }
        
        
    }
}

void PauseState::update(){
    
}

// Print assets to output
void PauseState::draw(){
    this->localGameData->window.clear(sf::Color::Black); // Not sure what this does but they have Red in the Flappy Bird version
    
    this->localGameData->window.draw(this->background);
    this->localGameData->window.draw(this->quitButton);
    this->localGameData->window.draw(this->instructionsButton);
    this->localGameData->window.draw(this->playButton);
    
    this->localGameData->window.display();
}